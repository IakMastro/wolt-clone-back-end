FROM node:lts-alpine

WORKDIR /api

COPY api/package*.json /api/

RUN npm install

COPY api/* /api/

EXPOSE 5000
CMD ["npm", "start"]