import { Controller, Get } from '@nestjs/common';
import { Restaurant } from 'lib/restaurant';
import { RestaurantsService } from './restaurants.service';

@Controller('restaurants')
export class RestaurantsController {
  constructor(private restaurantService: RestaurantsService) {}

  @Get()
  async getRestaurants(): Promise<Restaurant[]> {
    return this.restaurantService.getAll()
  }
}
