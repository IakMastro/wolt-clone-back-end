import { Injectable } from '@nestjs/common';
import { Restaurant } from 'lib/restaurant';

@Injectable()
export class RestaurantsService {
  private readonly restaurants: Restaurant[] = [
    {id: 1, name: "To Souvlaki tou Maki", logo: "placeholder", type: "souvlaki", price: "cheap"},
    {id: 2, name: "Pizza Fan", logo: "placeholder", type: "pizza", price: "medium"},
    {id: 3, name: "Bar Bee Kiou", logo: "placeholder", type: "burger", price: "expensive"}
  ]

  getAll(): Restaurant[] {
    return this.restaurants
  }
}
